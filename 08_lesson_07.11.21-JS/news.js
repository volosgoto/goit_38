// fetch(`https://content.guardianapis.com/search?page=3&q=debate&api-key=${API_KEY}`)
//     .then(resp => resp.json())
//     .then(data => console.log(data.response.results))

class News {
    constructor(URI) {
        this.URI = URI;
        this.list = document.querySelector(".list");
        this.btnPrev = document.querySelector("#prev");
        this.btnNext = document.querySelector("#next");
        this.input = document.querySelector("#input");
        this.span = document.querySelector("span");
        this.pageCounter = 1;
        this.totalPages = null;
        this.inputSearch = document.querySelector("form");
        this.inputValue = "";
    }

    fetchNews = () => {
        const query = `&q=${this.inputValue}`;
        let url = this.URI + this.pageCounter + query;
        fetch(url)
            .then((resp) => resp.json())
            .then((data) => {
                if (data.response.status == "ok") {
                    this.renderNews(data.response.results);
                    // console.log(data.response)
                    this.totalPages = data.response.pages;

                    this.renderPagination(data.response);
                }
            })
            .catch((error) => console.log(error));
    };

    renderPagination = (response) => {
        this.input.value = response.currentPage;
        this.span.textContent = `из ${response.pages}`;
    };

    renderNews = (resultsArray) => {
        this.list.innerHTML = "";
        const collectionNews = resultsArray.map(
            ({ webPublicationDate, webTitle, webUrl }) => {
                let li = document.createElement("li");
                let a = document.createElement("a");
                let p = document.createElement("p");

                a.href = webUrl;
                a.target = "_blank";
                a.textContent = webTitle;

                p.textContent = `Date: ${webPublicationDate}`;

                a.append(p);
                li.append(a);
                return li;
            }
        );

        this.list.append(...collectionNews);
    };

    loadListeners = () => {
        window.addEventListener("load", this.fetchNews);
        this.btnPrev.addEventListener("click", this.onPrevBtnClick);
        this.btnNext.addEventListener("click", this.onNextBtnClick);
        this.input.addEventListener("input", this.onInputChange);
        this.inputSearch.addEventListener("submit", this.onSearchChange);
    };

    onSearchChange = (evt) => {
        evt.preventDefault();
        this.inputValue = evt.target.elements.text.value.toLowerCase();
        this.fetchNews();
        // console.log(evt.target.elements);
    };

    onPrevBtnClick = () => {
        if (1 == this.pageCounter) {
            this.btnPrev.disabled = true;
            return;
        }
        this.btnPrev.disabled = false;
        this.pageCounter -= 1;
        this.input.value = this.pageCounter;

        this.fetchNews();
    };
    onNextBtnClick = () => {
        this.btnNext.disabled = true;

        if (this.pageCounter <= this.totalPages) {
            this.btnNext.disabled = false;
            this.pageCounter += 1;
            this.fetchNews();
            return;
        }
    };
    onInputChange = (evt) => {
        if (
            evt.target.value == "" &&
            evt.target.value == 0 &&
            evt.target.value > this.totalPages
        ) {
            return;
        }
        const inputValue = evt.target.value;
        this.pageCounter = Number(inputValue);
        this.fetchNews();
    };

    init = () => {
        this.loadListeners();
        //  console.log(this.btnPrev)
        //  console.log(this.btnNext)
        //  console.log(this.input)
        //  console.log(this.span)
    };
}

const API_KEY = "91b797ac-a72e-478d-81d5-df48ab119b65";
const URI = `https://content.guardianapis.com/search?q=react&api-key=${API_KEY}&page=`;

const news = new News(URI);
news.init();

// currentPage: 3
// pageSize: 10
// orderBy: "relevance"
// pages: 15647
// results: (10) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}]
// startIndex: 21
// status: "ok"
// total: 156469
// userTier: "developer"
