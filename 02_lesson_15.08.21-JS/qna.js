//    ... Rest      =    ... Spred

// let citiesOfUkraine = ['Kuiv', 'Lviv', 'Dnipro', 'Odesa'];
// let citiesOfEurop = ['London', 'Paris', 'Milan', 'Berlin'];

// let numbers = [1, 2, 3, 4];

// let cities = [
//     ...numbers,
//     'New york',
//     ...citiesOfEurop,
//     ...citiesOfUkraine,
//     'Giga',
// ];

// console.log(cities);
// console.log(cities === citiesOfUkraine);

// let qwe = [...citiesOfUkraine];

// ['Kuiv', 'Lviv', 'Dnipro', 'Odesa'];

// let newArr = [citiesOfUkraine];

// console.log(newArr);
// console.log(newArr[0][1]);

// citiesOfUkraine.push('London', 123, 'Vova');
// console.log(citiesOfUkraine);

// function myPush(sveta, newItem) {
//     return [newItem, ...sveta];
// }

// console.log(myPush(citiesOfUkraine, 'Pizza'));

// ==========================================================

// Spred
// let citiesOfUkraine = ['Kuiv', 'Lviv', 'Dnipro', 'Odesa', 'Berlin'];
// let cities = [...citiesOfUkraine, 'Berlin'];

// console.log(citiesOfUkraine === cities);
// console.log(cities);

// Rest
// let [AA, BT, ...pizza] = citiesOfUkraine;
// let [, , DN, ...rest] = citiesOfUkraine;

// console.log(DN, rest);

let vova = {
    name: 'Vova',
    age: 30,
    status: 'Admin',
};

let sara = {
    ...vova,
    name: 'Sara',
    car: 'BMW',
};

console.log(vova);
console.log(sara);
