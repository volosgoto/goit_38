/*
String
Number
Boolean
null
undefined

BigInt
Symbol

Object
*/

// undefined - возвращает комп когла не может найти
// let a;
// console.log(typeof b);
// console.log(typeof a);

// null - нету значения
// let a = null;
// console.log(typeof a); // object а самом деле null

// let btn = document.querySelector('button');
// console.log(btn);

// btn.addEventListener('click', () => {
//     console.log('Cliick the button');
// });

// 0, 1
// yes, no
// boy, girl
// true, false
// on, off

// if (){
// } else {
// }

// ==
// ===
// <
// >
// <=
// >=
// ! - NOT
// ||
// &&
// ~

// if (Boolean) {

// }

// if (5 > 10) {
//     console.log('Hello');
// } else {
//     console.log('Boolean False');
// }

// False Values
// null
// undefined
// 0
// ''
// NaN
// false

// || Зaтыкается на true
// console.log(10 < 5 || 10 || false);
// console.log( true || true)

// && Зaтыкается на false, если нету false, то возвращает последннее true
// console.log(5 && 20);
// console.log(5 && !!(10 < 2) && 20);

/////////////////////////////////////////////////////////////
// BigInt

// e * 10 - 53;
// e * 10 + 53;

// let num = 34569834523849563478563478563478534785634785634578463578345684735345n;
// console.log(typeof num);

///////////////////////////////////////////////////////
// Symbol
// let mySym = Symbol('qwe');
// console.log(typeof mySym);

// let ar = [12, 3, 4, 5];
// console.log(typeof ar);
// console.log(Array.isArray(ar));

// let user = { name: 'Vova', age: 25 };
// console.log(Array.isArray(user));
// console.log(typeof user);

/////////////////////////////////////////////////////////////
// console.log(typeof (5 + '25px'));
// console.log(typeof (5 / '25px'));
// console.log(5 / '0px'); // NaN
// console.log(typeof (-5 / '0')); // Infinity / -Infinity

/*
+ concatention
+ summ
- sub
* mult
/ div
*/

// Type hinting
// 1. String
// 2. Number
// 3. Boolean

///////////////////////////////////////////////////////////
// Arrays
// let animals = ['cat', 'dog', 'lion'];

// let melon = {
//     price: 10,
//     color: 'green',
//     weigth: 12,
// };

// let fruits = ['melon', 'apple', ['melon', 'apple', 'banana', 'pear'], 'pear'];

// let user = { name: 'Vova', age: 35, status: 'Admin', isLogin: false };

// let box = [
//     'pizza',
//     100,
//     fruits,
//     false,
//     user,
//     500,
//     null,
//     undefined,
//     BigInt,
//     'Vova',
//     'Sara',
//     'Pizza',
// ];

// box[0];
// box[1];
// box[3];
// console.log(box[0]);
// console.log(box[1]);
// console.log(box[2]);
// console.log(box[2][3]);
// console.log(box[2][0]);
// console.log(box[2]);
// console.log(box[2][2][1]);
// console.log(box[box.length - 1]);
// box[1] = 200;
// box[0] = 'Gamburger';

// console.log(box);
// console.log(arr);

//===================================================

// let user = { name: 'Vova', age: 35, status: 'Admin', isLogin: false };

// console.log(user.name);
// console.log(user.status);

// user.name = 'Sara';
// user.age = 18;
// console.log(user);

// user.car = {
//     manufacturer: 'BMW',
//     model: 'X5',
//     year: 2020,
//     tyres: {
//         season: 'summer',
//         raduis: 20,
//     },
//     hobbies: ['beer', 'football', 'dota', 'fishing'],
// };

// console.table(user.car.model);

// user.car.tyres.season = 'winter';

// console.log(user.car.tyres.season);

// console.log(user.car.hobbies[3]);

// let user = { name: 'Vova', age: 35, status: 'Admin', isLogin: false };

// user['age'];

// console.log(user['status']);
// console.log(user['isLogin']);

// =================================
// let vakcines = ['Phizer', 'AstraZeneca', 'Coronvac', 'Sputnic'];
// let countries = ['Usa', 'Britain', 'China', 'Russia'];

// function vakcinesBank(arrValues, arrKeys) {
//     let obj = {};

//     for (let i = 0; i < arrKeys.length; i++) {
//         obj[arrKeys[i]] = arrValues[i];
//     }
//     return obj;
// }

// console.log(vakcinesBank(vakcines, countries));

// ====================================================
// let vakcines = ['Phizer', 'AstraZeneca', 'Coronvac', 'Sputnic'];
// let countries = ['Usa', 'Britain', 'China', 'Russia'];

// for (let vac of vakcines) {
//     console.log(vac);
// }

// let resultObj = {};

// for (let idx in vakcines) {
// console.log(vakcines[idx]);
// resultObj[vakcines[idx]] = countries[idx];
// }

// console.log(resultObj);

// vakcinesBank(){

// }

// console.log(vakcinesBank(vakcines, countries));

// =====================================================

// let handelInput = (value, key) => {
//     return {
//         [key]: value,
//     };
// };

// let passwordObj = handelInput('123456', 'password');
// let emaildObj = handelInput('vova@i.ua', 'email');

// let user = {
//     passwordObj,
//     emaildObj,
// };

// console.log(user);

// =================================================
