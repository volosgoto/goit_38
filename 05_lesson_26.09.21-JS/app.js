// console.log("Hello Vova");

// let name = "Sara";

// function sayHello(userName) {
//     console.log(userName);
// }

// sayHello(name);

let express = require("express");
let users = require("./users");

console.log(users);

let app = express();

app.get("/", (req, res) => {
    res.send("<h1>Home page</h1>");
});

app.get("/about", (req, res) => {
    res.send("<h1>About page</h1>");
});

app.get("/contacts", (req, res) => {
    res.send("<h1>Contacts page</h1>");
});

app.get("/users", (req, res) => {
    res.status(200).json(users);
});

let PORT = 3001;
app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
});
