// Hoisting - это поднятие переменной
// Hosting - это размещение сайта в интернете

// lap_1. let sayHello
// lap_2. sayHello = function () {
//     console.log("Hello");
// };

// lap_1.
// let a;
// let b;
// let c;

// lap_2
// a = undefined;
// b = 12;
// c = "asdasd";

// ===============================================

// Не важно где ф-ция написана, важно где она вызвана
// Не важно где ф-ция написана, важно кто ее вызвал

// Functions === JS => TRUE

// function getName
// let userName
// const PI
// var

// let result = "asdas";
// let result = 231
// let result = null;
// let result = undefined;
// let result = [1,2,3];
// let result = {};
// let result = function(){};

// console.log(typeof result);

// sayHello();
// sayHello();

// Function Declaration
// Classic Function
// function sayHello() {
//     console.log("Hello");
// }

// Function Exspretion
// let sayHello = function () {
//     console.log("Hello");
// };

// console.log(sayHello());

// sayHello();
// sayHello();

// let a = "a";
// console.log(a);
// =============================================

// let sayHello = function () {
//     console.log("Hello");
// };

// sayHello();

// 1. lap_1
// let a;
// let b;
// let c;
// let pizza;

// 2. lap_2

// let a;
// let b = 12;
// let c = "asdasd";
// let pizza;

// console.log(pizza);
// ======================================
// return

// lap_1. let sayHello
// lap_2. sayHello = function () {
//     console.log("Hello");
// };
// lap_3. Function execution

// sayHello() - явный вызов
// sayHello - вызов результате действия (callback), ответочка

// function sayHello(name) {
// console.log("Hello Vova");
// return "Hello" + " " + name.toUpperCase();
// return undefined
// }

// {
//     sdf
//     sdrsdf
//     sdfsdfsdf

//     dsfsdfsdfsf
//     dsfsdfsdfsdfsd
// sdfsdfsdf

//     sdfsdfsdfs

//     return
// }

// let result = sayHello("sara");
// console.log(result);

// sayHello("bob");
// console.log(sayHello("vova"));
// ====================================================

// function sum(a, b) {return a + b}
// let sum = (a, b) => a + b;
// let sub = (a, b) => a - b;
// let mult = (a, b) => a * b;
// let div = (a, b) => a / b;

// function calck(pizza, gamburger, callback) {
//     return callback(pizza, gamburger);
// }

// calck();

// let result = calck(5, 10, div);
// console.log(result);

// 5 + 5 = 10
// 5 * 10 = 50

// ==========================================

// let name = "Vova";
// let age = 30;

// function sayHello() {
//     console.log("Hello");
// }

// let user = {
//     name: "Vova",
//     age: 30,

//     sayHello() {
//         console.log(this.name, this.age);
//     },
// };

// user.sayHello();

// let sara = {
//     name: "Sara",
//     age: 50,

//     setName(newName) {
//         this.name = newName;
//     },
// };

// sara.setName("Sara +++++");
// console.log(sara);
// sara.setName.call(user, "Vova++++++");
// console.log(user);

// sara.setName("Aunt Sara");
// user.sayHello();
// user.sayHello.call(sara);

// sara.sayHello();
// console.log(sara);

// console.dir(user);
// console.log(user);

// ============================================
// let arr = [23, 100, -34, 5, 50, -100, 34, 200, 6, -5];
// let min = arr[0];

// for (let item of arr) {
//     if (item < min) {
//         min = item;
//     }
// }
// console.log(min);

// let result = Math.min(20, 5, 10, 100, 45);
// let result = Math.max(...arr);

// let result = Math.max.bind(Math, ...arr);
// console.log(result());

// const bmw = {
//     title: "BMW",
//     model: "X5",
//     year: 2018,
// };

// const audi = {
//     title: "Audi",
//     model: "TT",
//     year: 2005,
// };

// const toyota = {
//     title: "Toyota",
//     model: "Camry",
//     year: 2021,
// };

// let carInfo = {
//     showTotaInfo() {
//         console.log(`${this.title}, ${this.model}, ${this.year}`);
//     },

//     setTitle(newTitle) {
//         return (this.title = newTitle);
//     },

//     setYear(newYaer) {
//         return (this.year = newYaer);
//     },
// };

// carInfo.showTotaInfo.call(bmw);
// carInfo.showTotaInfo.apply(bmw);
// carInfo.showTotaInfo.bind(bmw)();

// carInfo.setTitle.call(audi, "WAG");
// console.log(audi);

// Вызови мне метод setYear обьекта carInfo в контексте объекта toyota
// carInfo.setYear.call(toyota, 1980);
// console.log(toyota);
// ==========================================================

let germanCars = [
    {
        title: "BMW",
        model: "X5",
        year: 2018,
    },
    {
        title: "Audi",
        model: "TT",
        year: 2005,
    },
    {
        title: "Mercedes",
        model: "600d",
        year: 2021,
    },
];

let AmericanCars = [
    {
        title: "Ford",
        model: "Fusion",
        year: 2017,
    },
    {
        title: "Dodge",
        model: "Ram",
        year: 2000,
    },
    {
        title: "Chevrolet",
        model: "Suburban",
        year: 2006,
    },
];

let carInfo = {
    showTotaInfo() {
        console.log(`${this.title}, ${this.model}, ${this.year}`);
    },

    setTitle(newTitle) {
        return (this.title = newTitle);
    },

    setYear(newYaer) {
        return (this.year = newYaer);
    },

    // addProperty(propName, propValue, autosalon) {
    //     for (let car of autosalon) {
    //         console.log(car);
    //         car[propName] = propValue;
    //     }
    // },

    addProperty(propName, propValue) {
        for (let car of this) {
            // console.log(car);
            car[propName] = propValue;
        }
    },
};

// carInfo.addProperty.call(germanCars, "color", "white", germanCars);
// carInfo.addProperty.call(germanCars, "color", "white");
// console.log(germanCars);

carInfo.addProperty.call(AmericanCars, "tyres", "GoodYear");
console.dir(AmericanCars);
let bindedAmCar = carInfo.addProperty.bind(AmericanCars, "color", "green");
// AmericanCars.addProperty("milage", 100000);

// console.dir(AmericanCars);

// for (let { title, model, year } of autosalon) {
//     // let { title, model, year } = car;
//     // console.log(car);
//     if (title === "Audi") {
//         title = "WAG";
//         // carInfo.showTotaInfo.call(car);
//     }
// }

// for (let car of autosalon) {
//     // console.log(car);
//     if (car.title === "Audi") {
//         carInfo.showTotaInfo.bind(car)();
//     }
// }

// console.log(autosalon);

// carring
// asdasd()()()()();

// const bmw = {
//     title: "BMW",
//     model: "X5",
//     year: 2018,
// };

// bmw.tyres = "Pirelli";
// bmw.color = "black";

// bmw["tyres"] = "Brigestone";
// // [key] = value
// bmw["color"] = "Blue";

// console.log(bmw);
