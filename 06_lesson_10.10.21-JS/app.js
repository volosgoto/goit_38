const nameRef = document.querySelector(".js-feedback-name");
const emailRef = document.querySelector(".js-feedback-email");
const textAreaRef = document.querySelector(".js-feedback-comment");
const formRefs = document.querySelector(".js-feedback-form");
// console.log(nameRef);
// console.log(emailRef);
// console.log(formRefs);
// console.log(textAreaRef);

// getFromLocalStorage();

getFromLocalStorage(nameRef);
getFromLocalStorage(emailRef);
getFromLocalStorage(textAreaRef);

function onFormSubmit(evt) {
    evt.preventDefault();
    const form = evt.target;

    const getLocal = localStorage.getItem("message");

    if (getLocal) {
        localStorage.removeItem("message");
        form.reset();
    }
}

function getInputValue(elentName) {
    let inputText = elentName.target.value;
    saveToLS(elentName.target.name, inputText);
}

function saveToLS(key, itemValue) {
    localStorage.setItem(key, JSON.stringify(itemValue));
}

// function onFormSubmit(evt) {
//     evt.preventDefault();
//     // console.log('submit');
//     // console.log(form.elements);
//     // console.log(form.elements.message.value);
//     const form = evt.target;
//     const getLocal = localStorage.getItem("message");

//     if (getLocal) {
//         localStorage.removeItem("message");
//         form.reset();
//     }
// }

function onTextareaInput(evt) {
    console.log(evt.target.value);
    const textValue = evt.target.value;
    localStorage.setItem("message", textValue);
    // console.log(evt.target.name);
    // console.log(evt.target.placeholder);
    // console.log(evt.target.cols);
}

function getFromLocalStorage(formElem) {
    const keyFromLS = localStorage.getItem(formElem.name);
    if (keyFromLS) {
        formElem.value = JSON.parse(keyFromLS);
    }
}

// function getFromLocalStorage() {
//     const message = localStorage.getItem("message");
//     if (message) {
//         textAreaRef.textContent = message;
//     }
// }

formRefs.addEventListener("submit", onFormSubmit);
textAreaRef.addEventListener("input", onTextareaInput);

// Local Storage exmaples
// ==========================================
// localStorage.getItem
// localStorage.setItem
// localStorage.removeItem
// localStorage.clear

// const userName = "Vova";
// localStorage.setItem('user', userName);
// const vova = localStorage.getItem('pizza');

// console.log(vova);
// ==============================================
// const cartSara = [
//     { id: 123, price: 100, title:'lemon' },
//     { id: 345, price: 200, title:'cherry' },
//     { id: 456, price: 250, title:'banana' }
// ]

// localStorage.setItem('cartSara',JSON.stringify(cartSara))
// const sara = localStorage.getItem('cartSara');

// if (sara) {
//     console.log(sara);
//     const saraObj = JSON.parse(sara);
//     console.log(saraObj)};
