const itemsData = [
    { id: 1, name: "banana", price: 28, amount: 100, category: "fruits" },
    { id: 2, name: "potato", price: 15, amount: 500, category: "vegetables" },
    { id: 3, name: "orange", price: 45, amount: 120, category: "fruits" },
    { id: 4, name: "cucumber", price: 20, amount: 440, category: "vegetables" },
];

const shop = {
    title: "Silpo",
    addrese: "Odessa",
    items: itemsData,

    showItems() {
        for (const item of this.items) {
            console.log(
                `id:${item.id},name:${item.name},price:${item.price},amount:${item.amount}, category:${item.category}`
            );
        }
    },

    // addItem(id, name, price, amount, category) {
    //     this.items.push({id, name, price, amount, category})
    //   },

    //best of thre best metod
    // addItem(newItem) {
    //     this.items = [...this.items, newItem];
    // },

    // addItem(newItem) {
    //     let length = this.items.length;
    //     this.items.splice(length,0,newItem);
    //  },

    // updateItem(productName, newName) {
    //     for (const item of this.items) {
    //         if (productName === item.name) {
    //             item.name = newName;
    //             return;
    //          }
    //         }
    //         console.log('item not found!');
    // },

    updateItem(productName, newName) {
        for (let i = 0; i < this.items.length; i += 1) {
            if (productName === this.items[i].name) {
                this.items[i].name = newName;
                return;
            }
        }
        console.log("item not found!");
    },

    //   remooveItem(productName) {
    //     for (const item of this.items) {
    //       if (productName === item.name) {
    //         let idx = this.items.indexOf(item);
    //         this.items.splice(idx, 1);
    //           return;
    //       }
    //     }
    //     console.log('item not found!');
    //   },

    remooveItem(productName) {
        for (let i = 0; i < this.items.length; i += 1) {
            if (productName === this.items[i].name) {
                this.items.splice(i, 1);
                return;
            }
        }
        console.log("item not found!");
    },
};
//shop.addItem({id:5, name: 'lemon', price: 50,amount:1440, category: 'fruits' })
//shop.addItem(5,'abricot',34,150,'fruits')
//shop.addItem();
//shop.updateItem('potato', 'potato++')
shop.remooveItem("potato");
shop.showItems();
