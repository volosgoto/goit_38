class Shop {
    constructor(title, addrese, items) {
        this.title = title;
        this.addrese = addrese;
        this.items = items;
    }
    showItems() {
        for (const item of this.items) {
            console.log(
                `id:${item.id},name:${item.name},price:${item.price},amount:${item.amount}, category:${item.category}`
            );
        }
    }

    addItem(newItem) {
        this.items = [...this.items, newItem];
    }

    updateItem(productName, newName) {
        for (const item of this.items) {
            if (productName === item.name) {
                item.name = newName;
                return;
            }
        }
        console.log("item not found!");
    }

    remooveItem(productName) {
        for (const item of this.items) {
            if (productName === item.name) {
                let idx = this.items.indexOf(item);
                this.items.splice(idx, 1);
                return;
            }
        }
        console.log("item not found!");
    }
}
