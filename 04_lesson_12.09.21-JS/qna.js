/* 
    Дано число, например 31. Проверьте, что это число не делится ни на одно другое число кроме себя самого и единицы. То есть в нашем случае нужно проверить, что число 31 не делится на все числа от 2 до 30. Если число не делится - выведите 'false', а если делится - выведите 'true'. 
*/

// CRUD
// C - create
// R - read
// U - update
// D - delete

// let user = 'Vova' C
// console.log(user) R
// user = 'Sara' U
// delete user D

// let people = [
//     { id: 1, name: "Vova", age: 22, budget: 4000, isComlete: true },
//     { id: 2, name: "Sara", age: 30, budget: 3400, isComlete: false },
//     { id: 3, name: "Mike", age: 45, budget: 1500, isComlete: true },
//     { id: 4, name: "Bob", age: 18, budget: 400, isComlete: false },
// ];

// OOP
// Object oriental programing
// Объектно ориентированное програмирование
// Объектно ориентировая парадигма
// Объектно ориентированный подход

// Абстакция
// Объект(Сlass) => Экземпляр (instance)

// Наследование
// Полиморфизм
// Инкапсуляция

// Object (Class)

// class Array {}
// let arr = new Array();
// console.log([1, 2, 5] instanceof Array);

// istance - Экземпляр, объект сущьность
// class Shop {}
// let silpo = new Shop();
// console.log(silpo instanceof Shop);

// class Pizza {}
// let carbonara = new Pizza();
// console.log(carbonara instanceof Pizza);

// class UkrainianPizza extends Pizza {
//     constructor() {
//         super();
//     }
// }

// let kozakPizza = new UkrainianPizza();
// console.log(kozakPizza instanceof UkrainianPizza);

// class Team {
// constructor(title) {
//     this.title = title;

// //    return {    this.title = title;}

// }

// showInfo() {
//     console.log(this.title);
// }

// 1. Вызывается constructor в классе
// 2. Создается пустой объект {}
// 3. Привязывается this.
// 4. Возвращается созданный объект
// }

// let dynamo = new Team("Dynamo");
// let bavaria = new Team("Bayern");

// dynamo.showInfo();
// bavaria.showInfo();

// function sum(a, b) {
//     console.log(a + b);
// }

// public static void main(){
//     main()

// }

// class User {
//     private int a = 10;
//     public double b = 15.5;

//     private function (params) {

//     }

// }

// class Phone {
//     constructor(brand, model, price) {
//         this.brand = brand;
//         this.model = model;
//         this.priced = price;
//     }
// }

// let samsung = new Phone("Samsung", "A10", 35000);
// let apple = new Phone("Apple", "X", 28000);
// let lg = new Phone("Lg", "QWE", 15000);

// // console.log(samsung.brand);
// // samsung.brand = "Samsung+++";
// // console.log(samsung.brand);
// samsung.color = "black";
// samsung.article = "12312aDFSDSVF32EFZDF";
// console.log(samsung);
// console.log(apple);
// console.log(lg);

// =============================================
// Inharitance
class User {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    sayhello() {
        console.log(`Hello ${this.firstName} ${this.lastName}`);
    }
}

let vova = new User("Vova", "Smith");
// vova.sayhello();

// ==================================

// class Customer extends User {
//     constructor(firstName, lastName, membership) {
// super(firstName, lastName);
//     this.membership = membership;
// }

// sayhello() {
//     console.log(
//         `Hello ${this.firstName} ${this.lastName} ${this.membership}`
//     );
// }

// getInfo() {
//     super.sayhello();
// console.log(this.firstName);
// console.log(super.firstName);
// }
// }

// let sara = new Customer("Sara", "Connor", "Basic+");
// sara.sayhello();

// Must call super constructor in derived class before accessing 'this' or returning from derived constructor

// sara.getInfo();

class Admin extends User {
    constructor(firstName, lastName, age) {
        super(firstName, lastName);
        // super();
        this.age = age;
    }

    getAge() {
        console.log(this.firstName, this.age);
    }
}

let bob = new Admin("Bob", "Sponge", 30);
bob.getAge();
// bob.sayhello();
