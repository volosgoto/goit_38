// let userName;
// let userName = 'Vova';
// let userName = 'Sara';

// var userName = 'Vova';
// var userName = 'Sara';

// userName = 'Vova';
// userName = 'Sara';

// console.log(userName);

// const PI
// PI = 3.14
// console.log(PI);

// const IP = '192.1.208.54';
// console.log(IP);

// ===============================================
// Falsy values

// '' - empty string
// 0
// false
// undefined
// null
// NaN

// let userName = '';
// let num_1 = 0;
// // if (Boolean) {

// // }

// if (!!userName) {
//     console.log(`Hello ${userName}`);
// }

// Type Hinting
// String; +
// Number; +
// Boolean;

// let result = '16px' / 3;
// console.log(result);
// console.log(typeof result);

// console.log(-5 / 0);

// NaN  - это когда не получается математика
// -Infinity / Infinity  - это когда есть деление на ноль
// isNaN = это проверка получается ли математика

//===============================

// = - assigment - присваивание
// == - equal, сравние С приведением типов
// === - strict equal - равно, сравние БЕЗ приведения типов

// if (true) {
//     console.log('ok')

//     if (true){

//     } esle if(){

//     } else{qwe}

// } else {
//     console.log('Не верный пин')
// }

// //
// () ? asdasdasd : qwe

// :  console.log('Не верный пин')

// () ? () ? () ? true :  false :  false :  false

// let user = 'Sara'
// let result = (user) ? 'Пришел' : 'Не пришел'

// 5 + 5 + 5 = 15

// let str = 'abcdefg'; // c

// console.log(str[2]);
// console.log(str.length);

// let arr = [];
// let arr = ['vova', 'sara', 'bob'];
// console.log(arr[1]);
// console.log(arr.length);

// let name = 'Vova~~!e4';
// 0, 1, 2, 3;
// console.log(name.length);
// console.log(name[name.length - 1]);

// Китай
// Украина
// Германия
