// class User {
//     constructor(name, age) {
//         this.name = name;
//         this.age = age;
//     }

//     sayHello() {
//         // console.log(this);
//         console.log(`Hello ${this.name} you are ${this.age} old`);
//     }

//     // sayHello = () => {
//     //     console.log(`Hello ${this.name} you are ${this.age} old`);
//     // };
// }

// let vova = new User("Vova", 25);
// let sara = new User("Sara", 18);

// vova.sayHello();
// sara.sayHello();

// =======================================

// Calculator.displayPI();

// let currentDate = Date.now();
// console.log(currentDate);

// let min = new Math.min(100, 5, 20);
// console.log(min);

// class Calculator {
//     static PI = 3.14;

//     // static displayPI() {
//     //     console.log(this.PI);
//     // }

//     static sum(a, b) {
//         return a + b;
//     }

//     static circleSQRT(radius) {
//         console.log(this);
//         return this.PI + radius ** radius;
//     }

//     static displayPI = () => {
//         i;
//         console.log(this.PI);
//     };
// }

// let result = Calculator.sum(11, 20);
// result = Calculator.circleSQRT(20);
// result = new Calculator.PI();

// console.log(result);

// =========================================
// class SmartPhone {
//     constructor(productName, productPrice) {
//         this.productName = productName;
//         this.productPrice = productPrice;
//     }

//     battery = 4000;

//     // getInfo() {
//     //     console.log(`${this.productName} ${this.productPrice}`);
//     // }

//     getInfo = () => {
//         console.log(`${this.productName} ${this.productPrice}`);
//     };
// }

// let samsung = new SmartPhone("Samsung", 600);
// let iphone = new SmartPhone("Iphone", 1500);
// let nokia = new SmartPhone("Nokia", 500);

// samsung.getInfo();
// nokia.getInfo();

// console.log(samsung.battery);
// console.log(nokia.battery);

for (let i = 15; i >= 0; i--) {
    setTimeout(() => {
        console.log(1);
    }, 0);
}

function printNumbers(from, to) {
    let current = from;

    let timerId = setInterval(function () {
        alert(current);
        if (current == to) {
            clearInterval(timerId);
        }
        current++;
    }, 1000);
}

// использование:
printNumbers(5, 10);
